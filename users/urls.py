"""Defines URL patterns for Users."""
from django.urls import path, include
# from django.contrib.auth.views import login

from . import views

app_name = 'users'
urlpatterns = [
	# Login Page
	# path('login/', login, {'template_name': 'users/login.html'}, name='login'),

	path('logout/', views.logout_view, name='logout_view'),
	path('account/', include('django.contrib.auth.urls')),
	path('register/', views.register, name='register'),

]
