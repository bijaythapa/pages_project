from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import authenticate, login, logout


def logout_view(request):
	"""Log the User out."""
	logout(request)
	return HttpResponseRedirect(reverse('learning_logs:index'))


def register(request):
	"""Register a New User."""
	if request.method != 'POST':
		# create a blank form.
		form = UserCreationForm()
	else:
		# Process the complete form.
		form = UserCreationForm(data=request.POST)

		if form.is_valid():
			new_user = form.save()
			# Log the user in and redirect to home page.
			authenticated_user = authenticate(username=new_user.username, password=request.POST['password1'])
			login(request, authenticated_user)
			return HttpResponseRedirect(reverse('learning_logs:index'))

	context = {
		'form': form,
	}
	return render(request, 'registration/register.html', context)
