from django.forms import ModelForm

from .models import Pizza,Topping


class PizzaForm(ModelForm):
	"""docstring for ClassName"""
	class Meta:
		"""docstring for Meta"""
		model = Pizza
		fields = ['name',]
		labels = {
			'name': 'Enter Name: '
		}
	

class ToppingForm(ModelForm):
	"""docstring for ToppingForm"""
	class Meta:
		"""docstring for Meta"""
		model = Topping
		fields = ['name',]
		labels = {
			'name': 'Enter Name '
		}

