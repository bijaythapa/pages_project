from django.db import models
from django.contrib.auth.models import User


class Pizza(models.Model):
	"""a name of the pizza the user likes."""
	name = models.CharField(max_length=50)
	date_added = models.DateTimeField(auto_now_add=True)
	owner = models.ForeignKey(User, on_delete=models.CASCADE,)

	def __str__(self):
		"""return a string representation of model."""
		return self.name.title()


class Topping(models.Model):
	"""a name of topping the user favours on pizza."""
	pizza = models.ForeignKey(Pizza, on_delete=models.CASCADE,)
	name = models.CharField(max_length=70)
	date_added = models.DateTimeField(auto_now_add=True)

	class Meta:
		"""docstring for Meta"""
		verbose_name_plural = "toppings"


	def __str__(self):
		"""returns a string representation of model."""
		return self.name.title()

