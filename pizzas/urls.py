"""Defines URL patterns for pizzas app."""
from django.urls import path
from . import views

app_name='pizzas'

urlpatterns = [
	path('', views.index, name='index'),
	# Lists of the Pizza Types.
	path('pizzas/', views.pizzas, name='pizzas'),
	# Details of a Pizza Type.
	path('pizzas/<int:pizza_id>/', views.pizza, name='pizza'),
	# Add new Pizza Type
	path('add_pizza/', views.add_pizza, name='add_pizza'),
	# Add new topping in a Pizza-Type
	path('add_topping/<int:pizza_id>/', views.add_topping, name='add_topping'),
	# Edit the Toppings Details.
	path('edit_topping/<int:topping_id>/', views.edit_topping, name='edit_topping'),
]