from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.contrib.auth.decorators import login_required

from .models import Pizza, Topping
from .forms import PizzaForm, ToppingForm


def index(request):
	return render(request, 'pizzas/index.html')


@login_required
def pizzas(request):
	pizzas = Pizza.objects.order_by('id')
	context = {
		'pizzas': pizzas
	}
	return render(request, 'pizzas/pizzas.html', context)


@login_required
def pizza(request, pizza_id):
	"""Show a single Pizza type and all its Toppings."""
	pizza = Pizza.objects.get(id=pizza_id)
	toppings = pizza.topping_set.order_by('-date_added')
	context = {
		'pizza': pizza,
		'toppings': toppings,
	}
	return render(request, 'pizzas/pizza.html', context)


@login_required
def add_pizza(request):
	"""Add new Pizza type."""
	if request.method != 'POST':
		form = PizzaForm()
	else:
		form = PizzaForm(data=request.POST)
		if form.is_valid():
			form.save()
			return HttpResponseRedirect(reverse('pizzas:pizzas'))
	context = {
		'form': form,
	}
	return render(request, 'pizzas/new_pizza.html', context)


@login_required
def add_topping(request, pizza_id):
	"""Add new topping on you favourite Pizza."""
	pizza = Pizza.objects.get(id=pizza_id)

	if request.method != 'POST':
		form = ToppingForm()
	else:
		form = ToppingForm(data=request.POST)
		if form.is_valid():
			new_topping = form.save(commit=False)
			new_topping.pizza = pizza
			new_topping.save()
			print('data saved!!')
			return HttpResponseRedirect(reverse('pizzas:pizza', args=[pizza_id,]))

	context = {
		'pizza': pizza,
		'form': form,
	}
	return render(request, 'pizzas/new_topping.html', context)


@login_required
def edit_topping(request, topping_id):
	"""Edit the related Topping details."""
	topping = Topping.objects.get(id=topping_id)
	pizza = topping.pizza

	if request.method != 'POST':
		form = ToppingForm(instance=topping)
	else:
		form = ToppingForm(instance=topping, data=request.POST)
		if form.is_valid():
			form.save()
			return HttpResponseRedirect(reverse('pizzas:pizza', args=[pizza.id,]))

	context={
		'topping': topping,
		'pizza': pizza,
		'form': form,
	}
	return render(request, 'pizzas/edit_topping.html', context)

