from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect, Http404
# from django.core.urlresolvers import reverse
from django.urls import reverse
from django.contrib.auth.decorators import login_required

from .models import Topic, Entry
from .forms import TopicForm, EntryForm


def check_topic_owner(topic_owner, request_user):
	"""Checks if request user matches with topic owner."""
	if topic_owner != request_user:
		raise Http404


def index(request):
	"""The home page for learning logs."""
	return render(request, 'learning_logs/index.html')


@login_required
def topics(request):
	"""Show all topics."""
	# topics = Topic.objects.order_by('date_added')
	topics = Topic.objects.filter(owner=request.user).order_by('date_added')
	context = {
		'topics': topics,
	}
	return render(request, 'learning_logs/topics.html', context)


@login_required
def topic(request, topic_id):
	"""Show a single Topic and all its Entries."""
	topic = Topic.objects.get(id=topic_id)
	
	check_topic_owner(topic.owner, request.user)

	entries = topic.entry_set.order_by('-date_added')
	context = {
		'topic': topic,
		'entries': entries,
	}
	return render(request, 'learning_logs/topic.html', context)


@login_required
def new_topic(request):
	"""Add a new Topic."""
	if request.method != 'POST':
		# No data submitted, create a blank Form
		form = TopicForm()
	else:
		# POST data submitted, process data.
		form = TopicForm(request.POST)
		if form.is_valid():
			new_topic = form.save(commit=False)
			new_topic.owner = request.user
			new_topic.save()
			return HttpResponseRedirect(reverse('learning_logs:topics'))
			# return HttpResponseRedirect('learning_logs/topics')
			# return redirect('learning_logs/topics')
	
	context = {'form': form}
	return render(request, 'learning_logs/new_topic.html', context)


@login_required
def new_entry(request, topic_id):
	"""Add a new Entry for a particular Topic."""
	topic = Topic.objects.get(id=topic_id)
	if request.method != 'POST':
		# No data submitted. Create an Empty form.
		form = EntryForm()
	else:
		# POST data submitted. process data.
		form = EntryForm(data=request.POST)
		if form.is_valid():
			new_entry = form.save(commit=False)
			new_entry.topic = topic
			new_entry.save()
			return HttpResponseRedirect(reverse('learning_logs:topic', args=[topic_id,]))

	context = {
		'topic': topic,
		'form': form,
	}
	return render(request, 'learning_logs/new_entry.html', context)


@login_required
def edit_entry(request, entry_id):
	"""Edit an existing Entry."""
	entry = Entry.objects.get(id=entry_id)
	topic = entry.topic

	check_topic_owner(topic.owner, request.user)

	# if topic.owner != request.user:
	# 	raise Http404

	if request.method != 'POST':
		form = EntryForm(instance=entry)

	else:
		form = EntryForm(instance=entry, data=request.POST)
		if form.is_valid():
			form.save()
			return HttpResponseRedirect(reverse('learning_logs:topic', args=[topic.id,]))

	context = {
		'entry': entry,
		'topic': topic,
		'form': form,
	}
	return render(request, 'learning_logs/edit_entry.html', context)

