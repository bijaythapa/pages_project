from django import forms
# from django.forms import ModelForm
from .models import Topic, Entry

class TopicForm(forms.ModelForm):
# class TopicForm(ModelForm):
	"""docstring for TopicForm"""
	class Meta:
		"""docstring for Meta"""
		model = Topic
		fields = [
			'text',
			]
		labels = {
			'text': 'Enter your Topic',
			}


class EntryForm(forms.ModelForm):
	"""docstring for EntryForm"""
	class Meta:
		"""docstring for Meta"""
		model = Entry
		fields = [
			'text',
		]
		labels = {
			# 'text': 'Enter you learning details',
			'text': '',
		}
		widgets = {
			'text': forms.Textarea(
				attrs={
					'cols': 80,
				}
			)
		}
