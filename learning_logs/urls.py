"""Defines URL patterns for learning_logs."""
from django.urls import path
from . import views

app_name = 'learning_logs'
urlpatterns = [
	path('', views.index, name="index"),
	# url(r'^$', views.index, name"index"),
	# show all topics
	path('topics/', views.topics, name="topics"),
	# url(r'^topics/$', views.topics, name="topics"),
	# Detail page for single topic
	path('topics/<int:topic_id>/', views.topic, name="topic"),
	# url(r'^topics/(?P<topic_id>\d+)/$', views.topic, name="topic"),
	# Form page for new topic.
	path('new_topic/', views.new_topic, name="new_topic"),
	# url(r'^new_topic/$', views.new_topic, name="new_topic"),
	# Page for adding a new Entry.
	path('new_entry/<int:topic_id>/', views.new_entry, name="new_entry"),
	# url(r'^new_entry/(?P<topic_id>\d+)/$', views.new_entry, name="new_entry"),
	# Page for editing an Entry.
	path('edit_entry/<int:entry_id>/', views.edit_entry, name="edit_entry"),
	# url(r'^edit_entry/(?P<entry_id>\d+)/$', views.edit_entry, name="edit_entry"),
]
