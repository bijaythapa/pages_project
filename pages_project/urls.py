from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('admin/', admin.site.urls),
    path('pages/', include('pages.urls')),
    path('', include('learning_logs.urls')),
    path('pizzas/', include('pizzas.urls')),
    path('users/', include('users.urls')),
]
